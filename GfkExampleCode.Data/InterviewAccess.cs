﻿using System.Collections.Generic;
using GfkExampleCode.Model;
using System.Linq;

namespace GfkExampleCode.Data
{
    public class InterviewAccess
    {
        public List<Interview> Interviews()
        {
            List<Interview> interviews =
            (
             from record in AllInterviews()
             select new Interview
             {
                 Id = record.Id,
                 Type = record.Type,
                 Description = record.Description
             }).ToList();

            return interviews;
        }
        public Interview Interview(int interviewId)
        {

            InterviewRecord interview = AllInterviews().FirstOrDefault(i => i.Id == interviewId);

            Interview result = null;
            if (interview != null)
            {
                result = new Interview()
                {
                    Id = interview.Id,
                    Description = interview.Description,
                    Type = interview.Type
                };
            }

            return result;

        }

        public List<Question> QuestionsByInterviewId(int interviewId)
        {
            List<Question> questions =
                (
                from record in AllQuestions()
                where record.InterviewId == interviewId
                select new Question
                {
                    Id = record.Id,
                    SortId = record.SortId,
                    OneAnswerPossible = record.OneAnswerPossible,
                    Description = record.Description
                }).ToList();

            return questions;
        }

        public void SaveInterview(int userId, Interview interview)
        {
            //TODO: Save result for interview by userId
        }
    
        public List<Answer> AnswersByQuestionId(int questionId)
        {
            List<Answer> answers =
                (
                from record in AllAnswers()
                where record.QuestionId == questionId
                select new Answer
                {
                    Id = record.Id,
                    Choice = record.Choice,
                    Description = record.Description
                }).ToList();

            return answers;
        }

        private List<InterviewRecord> AllInterviews()
        {

            List<InterviewRecord> resultset = new List<InterviewRecord>
            {
                new InterviewRecord
                {
                    Id = 1,
                    Description = "Interview for the position of Full Stack Developer",
                    Type = "Full Stack Developer",
                },
                new InterviewRecord
                {
                    Id = 2,
                    Description = "Interview for the position of Medior Developer",
                    Type = "Medior Developer",
                }

            };

            return resultset;

        }

        private List<QuestionRecord> AllQuestions()
        {
            List<QuestionRecord> resultset = new List<QuestionRecord>
                    {
                        new QuestionRecord
                        {
                            Id = 1,
                            SortId = 1,
                            InterviewId = 1,
                            OneAnswerPossible = true,
                            Description = "How did you find out about this job opportunity?",
                        },
                        new QuestionRecord
                        {
                            Id = 2,
                            SortId = 2,
                            InterviewId = 1,
                            OneAnswerPossible = false,
                            Description = "How do you find the company’s location?",
                        },
                        new QuestionRecord
                        {
                            Id = 3,
                            SortId = 3,
                            InterviewId = 1,
                            OneAnswerPossible = true,
                            Description = "What was your impression of the office where you had the interview?",
                        },
                        new QuestionRecord
                        {
                            Id = 4,
                            SortId = 4,
                            InterviewId = 1,
                            OneAnswerPossible = true,
                            Description = "How technically challenging was the interview?",
                        },
                        new QuestionRecord
                        {
                            Id = 5,
                            SortId = 5,
                            InterviewId = 1,
                            OneAnswerPossible = false,
                            Description = "How can you describe the manager that interviewed you?",
                        }
                    };

            return resultset;
        }

        private List<AnswerRecord> AllAnswers()
        {
            List<AnswerRecord> resultSet = new List<AnswerRecord>
            {
                // answers to question 1
                new AnswerRecord
                {
                    Id = 1,
                    QuestionId = 1,
                    Choice = false,                    
                    Description = "StackOverflow",
                },
                new AnswerRecord
                {
                    Id = 2,
                    QuestionId = 1,
                    Choice = false,                    
                    Description = "Indeed",
                },
                new AnswerRecord
                {
                    Id = 3,
                    QuestionId = 1,
                    Choice = false,                    
                    Description = "Other",
                },
                // answers to question 2
                new AnswerRecord
                {
                    Id = 1,
                    QuestionId = 2,
                    Choice = false,
                    Description = "Easy to access by public transport.",
                },
                new AnswerRecord
                {
                    Id = 2,
                    QuestionId = 2,
                    Choice = false,
                    Description = "Easy to access by car.",
                },
                new AnswerRecord
                {
                    Id = 3,
                    QuestionId = 2,
                    Choice = false,
                    Description = "In a pleasant area.",
                },
                new AnswerRecord
                {
                    Id = 4,
                    QuestionId = 2,
                    Choice = false,
                    Description = "None of the above.",
                },
                // answers to question 3
               new AnswerRecord
                {
                    Id = 1,
                    QuestionId = 3,
                    Choice = false,
                    Description = "Tidy.",
                },
                new AnswerRecord
                {
                    Id = 2,
                    QuestionId = 3,
                    Choice = false,
                    Description = "Sloppy",
                },
                new AnswerRecord
                {
                    Id = 3,
                    QuestionId = 3,
                    Choice = false,
                    Description = "Did not notice",
                },
                // answers to question 4
               new AnswerRecord
                {
                    Id = 1,
                    QuestionId = 4,
                    Choice = false,
                    Description = "Very difficult.",
                },
                new AnswerRecord
                {
                    Id = 2,
                    QuestionId = 4,
                    Choice = false,
                    Description = "Difficult",
                },
                new AnswerRecord
                {
                    Id = 3,
                    QuestionId = 4,
                    Choice = false,
                    Description = "Moderate",
                },
                new AnswerRecord
                {
                    Id = 4,
                    QuestionId = 4,
                    Choice = false,
                    Description = "Easy",
                },
                // answers to question 5
               new AnswerRecord
                {
                    Id = 1,
                    QuestionId = 5,
                    Choice = false,
                    Description = "Enthusiastic",
                },
                new AnswerRecord
                {
                    Id = 2,
                    QuestionId = 5,
                    Choice = false,
                    Description = "Polite",
                },
                new AnswerRecord
                {
                    Id = 3,
                    QuestionId = 5,
                    Choice = false,
                    Description = "Organized",
                },
                new AnswerRecord
                {
                    Id = 4,
                    QuestionId = 5,
                    Choice = false,
                    Description = "Could not tell",
                },


            };

            return resultSet;
        }
    }
}
