﻿
using GfkExampleCode.Data;
using GfkExampleCode.Model;
using System.Collections.Generic;
using System.Linq;

namespace GfkExampleCode.Domain
{
    public class Answers
    {
        private List<Answer> _answers;

        public Answers(int questionId)
        {
            _answers = new InterviewAccess().AnswersByQuestionId(questionId);
        }

        public List<Answer> Data
        {
            get
            {
                return _answers;
            }
        }

        public bool Exists
        {
            get
            {
                return _answers.Any();
            }
        }

    }
}
