﻿using GfkExampleCode.Data;

namespace GfkExampleCode.Domain
{
    public class Interview
    {
        private Model.Interview _interview;

        public Interview(){}

        public Interview(int interviewId)
        {
            _interview = new InterviewAccess().Interview(interviewId);

            var questions = new Questions(interviewId);
            if (questions.Exists)
            {
                _interview.Questions.AddRange(questions.Data);
            }
            
        }

        public Model.Interview Data
        {
            get
            {
                return _interview;
            }
        }

        public void Save(int userId, Model.Interview interview)
        {

            var interviewAccess = new InterviewAccess();
            interviewAccess.SaveInterview(userId, interview);

        }

        public bool Exists
        {
            get
            {
                return _interview != null;
            }
        }

    }
}
