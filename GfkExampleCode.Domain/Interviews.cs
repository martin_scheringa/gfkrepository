﻿using GfkExampleCode.Data;
using System.Collections.Generic;

namespace GfkExampleCode.Domain
{
    public class Interviews
    {
        public List<Model.Interview> Data
        {
            get
            {
                return new InterviewAccess().Interviews();
            }
        }
    }
 
}
