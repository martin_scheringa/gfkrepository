﻿using GfkExampleCode.Data;
using GfkExampleCode.Model;
using System.Collections.Generic;
using System.Linq;

namespace GfkExampleCode.Domain
{
    class Questions
    {
        private List<Question> _questions;

        public Questions(int interviewId)
        {
            _questions = new InterviewAccess().QuestionsByInterviewId(interviewId);

            foreach(var question in _questions)
            {
                var answers = new Answers(question.Id);

                if (answers.Exists)
                {
                    question.Answers.AddRange(answers.Data);
                }

            }
        }

        public List<Question> Data
        {
            get
            {
                return _questions;
            }
        }

        public bool Exists
        {
            get
            {
                return _questions.Any();
            }
        }

    }
}
