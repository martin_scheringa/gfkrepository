﻿
using GfkExampleCode.Model;
using System.ComponentModel.DataAnnotations;


namespace GfkExampleCode.Web.Models
{
    public class AnswersGivenCheck : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            if (value != null)
            {
                var question = value as Question;

                var numberOfTrue = 0;
                foreach(var answer in question.Answers)
                {
                    if (answer.Choice)
                    {
                        numberOfTrue++;
                    }
                }

                if (numberOfTrue == 0 )
                {
                    var errorMessage = string.Format("Question {0}: Please answer the question", question.Id);
                    return new ValidationResult(errorMessage);
                }


                if (question.OneAnswerPossible && numberOfTrue > 1)
                {
                    var errorMessage = string.Format("Question {0}: only one answer can be selected.", question.Id);
                    return new ValidationResult(errorMessage);
                }

            }

            return ValidationResult.Success;
        }

    }
}