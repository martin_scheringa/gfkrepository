﻿
using System.Collections.Generic;

namespace GfkExampleCode.Model
{
    public class Interview
    {
        public Interview()
        {
            Questions = new List<Question>();
        }

        public int Id { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public List<Question> Questions { get; private set; }

    }
}
