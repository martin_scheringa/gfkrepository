﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using GfkExampleCode.Web.Models;

namespace GfkExampleCode.Model
{
    [AnswersGivenCheck]
    public class Question
    {
        public Question()
        {
            Answers = new List<Answer>();
        }

        public int Id { get; set; }

        public int SortId { get; set; }

        public bool OneAnswerPossible { get; set; }

        public string Description { get; set; }
        
        public List<Answer> Answers { get; private set; }

    }
}
