﻿using GfkExampleCode.Domain;
using GfkExampleCode.Web.Models;
using System.Web.Mvc;

namespace GfkExampleCode.Web.Controllers
{
    public class InterviewController : Controller
    {
        // GET: Interview
        public ActionResult Index()
        {
            var model = new InterviewViewModel()
            {
                Interviews = new Interviews().Data
            };
            return View(model);
        }

        // GET: Interview/Details/5
        public ActionResult Details(int id)
        {
            var model = new InterviewViewModel()
            {
                Titel = "Interview full stack developer",
                Interview = new Interview(id).Data
            };
            return View(model);
        }

        // POST: Interview/Save
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Details([Bind(Include = "Interview")]InterviewViewModel viewModel)
        {
                if (ModelState.IsValid)
                {
                    var userId = 1; //TODO: Ophalen van huidige userId;
                    var interview = new Interview();
                     interview.Save(userId, viewModel.Interview);

                    return RedirectToAction("Index");

                }

            return View(viewModel);
  
        }

    }
}
