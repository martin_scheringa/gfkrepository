﻿using System.Collections.Generic;

namespace GfkExampleCode.Web.Models
{
    public class InterviewViewModel
    {

        public string Titel { get; set; }

        public List<Model.Interview> Interviews { get; set; }

        public Model.Interview Interview { get; set; }

     }
}