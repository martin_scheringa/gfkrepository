﻿namespace GfkExampleCode.Data
{
    class AnswerRecord
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public string Description { get; set; }
        public bool Choice { get; set; }

    }
}
