﻿using System.Collections.Generic;
using GfkExampleCode.Model;
using System.Linq;

namespace GfkExampleCode.Data
{
    public class InterviewAccess
    {
        public List<Interview> Interviews()
        {
            List<Interview> interviews =
            (
             from record in AllInterviews()
             select new Interview
             {
                 Id = record.Id,
                 Type = record.Type,
                 Description = record.Description
             }).ToList();

            return interviews;
        }
        public Interview Interview(int interviewId)
        {

            InterviewRecord interview = AllInterviews().FirstOrDefault(i => i.Id == interviewId);

            Interview result = null;
            if (interview != null)
            {
                result = new Interview()
                {
                    Id = interview.Id,
                    Description = interview.Description,
                    Type = interview.Type
                };
            }

            return result;

        }

        public List<Question> QuestionsByInterviewId(int interviewId)
        {
            List<Question> questions =
                (
                from record in AllQuestionsByInterviewId(interviewId)
                where record.InterviewId == interviewId
                select new Question
                {
                    Id = record.Id,
                    SortId = record.SortId,
                    OneAnswerPossible = record.OneAnswerPossible,
                    Description = record.Description
                }).ToList();

            return questions;
        }

        public List<Answer> AnswersByQuestionId(int questionId)
        {
            List<Answer> answers =
                (
                from record in AllAnswersByQuestionId(questionId)
                where record.QuestionId == questionId
                select new Answer
                {
                    Id = record.Id,
                    Choice = record.Choice,
                    Description = record.Description
                }).ToList();

            return answers;
        }

        private List<InterviewRecord> AllInterviews()
        {

            List<InterviewRecord> resultset = new List<InterviewRecord>
            {
                new InterviewRecord
                {
                    Id = 1,
                    Description = "Interview assignment for the position of Full Stack Developer",
                    Type = "Full Stack Developer",
                },
                new InterviewRecord
                {
                    Id = 2,
                    Description = "Interview assignment for the position of Medior Developer",
                    Type = "Medior Developer",
                }

            };

            return resultset;

        }

        private List<QuestionRecord> AllQuestionsByInterviewId(int interviewId)
        {
            List<QuestionRecord> resultset = new List<QuestionRecord>
                    {
                        new QuestionRecord
                        {
                            Id = 1,
                            SortId = 1,
                            InterviewId = 1,
                            OneAnswerPossible = true,
                            Description = "How did you find out about this job opportunity?",
                        },
                        new QuestionRecord
                        {
                            Id = 2,
                            SortId = 2,
                            InterviewId = 1,
                            OneAnswerPossible = true,
                            Description = "How do you find the company’s location?",
                        }
                    };

            return resultset;
        }

        private List<AnswerRecord> AllAnswersByQuestionId(int questionId)
        {
            List<AnswerRecord> resultSet = new List<AnswerRecord>
            {
                new AnswerRecord
                {
                    Id = 1,
                    Choice = false,
                    QuestionId = 1,
                    Description = "StackOverflow",
                },
                new AnswerRecord
                {
                    Id = 2,
                    Choice = false,
                    QuestionId = 1,
                    Description = "Indeed",
                },
                new AnswerRecord
                {
                    Id = 3,
                    Choice = false,
                    QuestionId = 1,
                    Description = "Other",
                }
            };

            return resultSet;
        }
    }
}
