﻿using System.Collections.Generic;

namespace GfkExampleCode.Data
{
    public class InterviewRecord
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
    }
}
