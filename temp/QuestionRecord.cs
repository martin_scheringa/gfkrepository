﻿using System.Collections.Generic;

namespace GfkExampleCode.Data
{
    public class QuestionRecord
    {
        public int Id { get; set; }

        public int InterviewId { get; set; }

        public int SortId { get; set; }

        public bool OneAnswerPossible { get; set; }

        public string Description { get; set; }

    }
}
